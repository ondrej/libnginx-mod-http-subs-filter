Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: ngx_http_substitutions_filter_module
Upstream-Contact: Weibin Yao <yaoweibin@gmail.com>
Source: https://github.com/yaoweibin/ngx_http_substitutions_filter_module

Files: *
Copyright: 2014, Weibin Yao <yaoweibin@gmail.com>
License: BSD-2-clause

Files: debian/*
Copyright: 2022, Miao Wang <shankerwangmiao@gmail.com>
 2022, 2023, Jan Mojzis <jan.mojzis@gmail.com>
License: BSD-3-clause

Files: test/*
Copyright: 2011, Antoine Bonavita "<antoine.bonavita@gmail.com>".
 2009-2011, agentzh "<agentzh@gmail.com>".
 2009-2011, Taobao Inc., Alibaba Group
License: BSD-3-clause

Files: test/inc/*
Copyright: agentzh (章亦春) "<agentzh@gmail.com>"
 Antoine BONAVITA "<antoine.bonavita@gmail.com>"
 2009-2011, Taobao Inc., Alibaba Group
License: BSD-3-clause

Files: test/inc/Module/Install.pm
Copyright: 2008-2011, Adam Kennedy.
License: BSD-3-clause

Files: test/lib/*
Copyright: agentzh (章亦春) "<agentzh@gmail.com>"
 Antoine BONAVITA "<antoine.bonavita@gmail.com>"
 2009-2011, Taobao Inc., Alibaba Group
License: BSD-3-clause

Files: test/lib/Test/Nginx.pm
Copyright: 2011, Antoine Bonavita C<< <antoine.bonavita@gmail.com> >>.
 2009-2011, agentzh C<< <agentzh@gmail.com> >>.
 2009-2011, Taobao Inc., Alibaba Group (L<http://www.taobao.com>).
License: BSD-3-clause

Files: test/lib/Test/Nginx/LWP.pm
Copyright: 2009-2011, agentzh C<< <agentzh@gmail.com> >>.
 2009-2011, Taobao Inc., Alibaba Group (L<http://www.taobao.com>).
License: BSD-3-clause

Files: test/lib/Test/Nginx/Socket.pm
Copyright: 2011, Antoine BONAVITA C<< <antoine.bonavita@gmail.com> >>.
 2009-2011, agentzh C<< <agentzh@gmail.com> >>.
 2009-2011, Taobao Inc., Alibaba Group (L<http://www.taobao.com>).
License: BSD-3-clause

Files: test/t/*
Copyright: agentzh (章亦春) "<agentzh@gmail.com>"
 Antoine BONAVITA "<antoine.bonavita@gmail.com>"
 2009-2011, Taobao Inc., Alibaba Group
License: BSD-3-clause

Files: test/test.sh
Copyright: agentzh (章亦春) "<agentzh@gmail.com>"
 Antoine BONAVITA "<antoine.bonavita@gmail.com>"
 2009-2011, Taobao Inc., Alibaba Group
License: BSD-3-clause

License: BSD-2-clause
 All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: BSD-3-clause
 All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. Neither the name of the University nor the names of its contributors
    may be used to endorse or promote products derived from this
    software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS
 BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
